package com.example.sumitdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SumitdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SumitdemoApplication.class, args);
	}

}
